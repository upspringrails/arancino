<?php
/**
 * @file views-view-grid.tpl.php
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php drupal_add_library('jquery_plugin', 'scrollable') ?>


<div id="image_wrap">
	<img src=""/>
	<div class="wrap"></div>
</div>

<div class="scroll-wrap">
<a class="prev browse left"></a>
<div class="scrollable" id="scrollable">
	<div class="items">
    <?php foreach ($rows as $row_number => $columns): ?>      		
        <?php foreach ($columns as $column_number => $item): ?>
			<div>
            <?php print $item; ?>	
     		</div>
        <?php endforeach; ?>
    <?php endforeach; ?>
	</div>
</div>
<a class="next browse right"></a>
</div>
	<script type="text/javascript"> 
	
	(function ($) {
		$(".scrollable").scrollable();
				
		
		$(".items div").click(function() {
			// see if same div is being clicked
			if ($(this).hasClass("active")) { return; }
			
			var imgSrc =  $(this).find("img");
			
			// calclulate large image's URL based on the thumbnail URL (flickr specific)
			var url = $(imgSrc).attr("src").replace("square_thumbnail", "large");

			// get handle to element that wraps the image and make it semi-transparent
			var wrap = $("#image_wrap").fadeTo("medium", 0.5);
			
			// the large image
			var img = new Image();
			
			// call this function after it's loaded
			img.onload = function() {

				// make wrapper fully visible
				wrap.fadeTo("fast", 1);

				// change the image
				wrap.find("img").attr("src", url);

			};
											
			// begin loading the image
			img.src = url;
			
			//remove old title
			$('#image_wrap h6').remove();
			
			// get title
			var imgTitle = $(this).find("h6");
			
			// place new title
			$(imgTitle).clone().appendTo('#image_wrap');
			
			
			// activate item
			$(".items div").removeClass("active");
			$(this).addClass("active");

		// when page loads simulate a "click" on the first image
		}).filter(":first").click();

	}(jQuery));
</script>

