jQuery(document).ready(function($) {
		
		var menuItems = $("#block-system-main-menu .block-content li").length;
		var menuExpandedItems = $("#block-system-main-menu .block-content .expanded li").length;
		var numberParentItems = menuItems - menuExpandedItems
		var navWidth = $("#block-system-main-menu").width();
		var setItemWidth = navWidth/numberParentItems;
		$("#block-system-main-menu .block-content li").css("width", setItemWidth);

		$(window).resize(function() {
		  var menuItems = $("#block-system-main-menu .block-content li").length;
			var menuExpandedItems = $("#block-system-main-menu .block-content .expanded li").length;
			var numberParentItems = menuItems - menuExpandedItems
			var navWidth = $("#block-system-main-menu").width();
			var setItemWidth = navWidth/numberParentItems;
			$("#block-system-main-menu .block-content li").css("width", setItemWidth);
		});
});
jQuery(window).load(function () {
    jQuery(window).resize(function () {
        jQuery('.views_slideshow_main img').each(function () {
            var img_height = jQuery(this).height();
            if (img_height !== 0) {
                jQuery('.views_slideshow_main').height(img_height);
                return false;
            }
        });
        
    });
});
