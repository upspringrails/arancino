<div id="wrapper">
	<!-- #header -->
	<div id="header">
		<!-- #header-inside -->
	    <div id="header-inside" class="container_12 clearfix">
	    	<!-- #header-inside-left -->
	        <div id="header-inside-left" class="grid_4">            
	          <?php if ($logo): ?>
						<div id="company-logo">
	            <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>" class="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
						</div>
	          <?php endif; ?>                 
            
	        </div><!-- EOF: #header-inside-left -->
        
	        <!-- #header-inside-right -->    
	        <div id="header-inside-right" class="grid_8">
					
						<?php if ($page['header']): ?>				
							<?php print render($page['header']); ?>						
						<?php endif; ?>											 			
						<div id="navigation" class="clearfix">
				      <?php if ($page['navigation']) :?>
				      <?php print drupal_render($page['navigation']); ?>
				      <?php else :
				      if (module_exists('i18n_menu')) {
				      $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
				      } else {
				      $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu')); 
				      }
				      print drupal_render($main_menu_tree);
				      endif; ?>
				     </div>
	        </div><!-- EOF: #header-inside-right -->	    		
	    </div><!-- EOF: #header-inside -->		
	</div><!-- EOF: #header -->

	<!-- #content -->
	<div id="content">
		<!-- #content-inside -->
	    <div id="content-inside" class="container_12 clearfix">
         
	  		<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
				
				<?php if(!$is_front):?>
					<?php if (theme_get_setting('breadcrumb_display','arancino')): print $breadcrumb; endif; ?>  
	        <?php if ($page['sidebar_second']) { ?>
	        <div id="main" class="grid_9">
						<?php } else { ?>
	        <div id="main" class="grid_12">    
	        <?php } ?>           	                                  
       			<div class="content-wrap">
	            <?php if ($messages): ?>
	            <div id="console" class="clearfix">
	            <?php print $messages; ?>
	            </div>
	            <?php endif; ?>
     
	            <?php if ($page['help']): ?>
	            <div id="help">
	            <?php print render($page['help']); ?>
	            </div>
	            <?php endif; ?>
            
	            <?php if ($action_links): ?>
	            <ul class="action-links">
	            <?php print render($action_links); ?>
	            </ul>
	            <?php endif; ?>
            
							<?php print render($title_prefix); ?>
	            <?php if ($title): ?>
	            	<h1 class="page-title"><?php print $title ?></h1>
	            <?php endif; ?>
	            <?php print render($title_suffix); ?>
            
	            <?php if ($tabs): ?><?php print render($tabs); ?><?php endif; ?>
            
	            <?php print render($page['content']); ?>            	            
            </div>
	        </div><!-- EOF: #main -->
        
	        <?php if ($page['sidebar_second']) :?>
	        <!-- #sidebar-second -->
	        <div id="sidebar-second" class="grid_3">
	        	<?php print render($page['sidebar_second']); ?>
	        </div><!-- EOF: #sidebar-second -->
	        <?php endif; ?>  
			
				<?php endif;?>
	    </div><!-- EOF: #content-inside -->
			 	
	</div><!-- EOF: #content -->

	<?php if ($page['footer_first'] || $page['footer_second']) :?>
	<!-- #footer -->    
	<div id="footer">
		<!-- #footer-inside -->
	    <div id="footer-inside" class="container_12 clearfix">
    
	        <div class="footer-area grid_5">
	        <?php print render($page['footer_first']); ?>
	        </div><!-- EOF: .footer-area -->
        
	        <div class="footer-area grid_7">
	        <?php print render($page['footer_second']); ?>
	        </div><!-- EOF: .footer-area -->
       
	    </div><!-- EOF: #footer-inside -->

	</div><!-- EOF: #footer -->
	<?php endif; ?>  
	<!-- #footer-bottom -->    
	<div id="footer-bottom">
		<!-- #footer-bottom-inside --> 
	    <div id="footer-bottom-inside" class="container_12 clearfix">
	    	<div class="footer-area grid_3">
	      <?php print render($page['footer_bottom_first']); ?>
	      </div><!-- EOF: .footer-area -->
      
	      <div class="footer-area grid_2">
	      <?php print render($page['footer_bottom_second']); ?>
	      </div><!-- EOF: .footer-area -->
      
	      <div class="footer-area grid_2">
	      <?php print render($page['footer_bottom_third']); ?>
	      </div><!-- EOF: .footer-area -->
	
				<div class="footer-area grid_2">
	      <?php print render($page['footer_bottom_fourth']); ?>
	      </div><!-- EOF: .footer-area -->
	
		  <div class="footer-area grid_3">
	      <?php print render($page['footer_bottom_fifth']); ?>
	      </div><!-- EOF: .footer-area -->

		  <div class="footer-area grid_12">
	      <?php print render($page['footer_last']); ?>
	      </div><!-- EOF: .footer-area -->
	
	    </div><!-- EOF: #footer-bottom-inside -->
			
	</div><!-- EOF: #footer -->
	<?php if ($page['background_blocks']): ?>
		<div id="background-zone">
			<?php print render($page['background_blocks']); ?>
		</div>
	<?php endif; ?>
</div>