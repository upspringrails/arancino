<?php
// to install, cd to website root, then run:
// curl -sSL -o dashboard_version_check.php https://dashboard.upspringhosting.net/dashboard_version_check.php.txt

// note: if our hosting plugin exists, it will return the versions itself and this file is not needed

echo "Version:1.0.5\n";
echo 'Hostname:', gethostname(), "\n";
echo 'PHP:', phpversion(), "\n";

if (file_exists('wp-includes/version.php')) {  // wordpress
  require_once 'wp-includes/version.php';
  echo 'WordPress:', $wp_version, "\n";
}

if (file_exists('core/lib/Drupal.php')) {  // drupal 8
  require_once 'core/lib/Drupal.php';
  echo 'Drupal:', Drupal::VERSION, "\n";
}
if (file_exists('includes/bootstrap.inc') || file_exists('modules/system/system.module')) {
  if (file_exists('includes/bootstrap.inc')) {  // drupal 7
    require_once 'includes/bootstrap.inc';
  }
  if (file_exists('modules/system/system.module')) {  // drupal 6
    require_once 'modules/system/system.module';
  }
  echo 'Drupal:', VERSION, "\n";
}

if (file_exists('../vendor/craftcms/cms/src/config/app.php')) {  // craft cms
  require_once '../vendor/yiisoft/yii2/base/Configurable.php';
  require_once '../vendor/yiisoft/yii2/base/BaseObject.php';
  require_once '../vendor/yiisoft/yii2/base/Component.php';
  require_once '../vendor/yiisoft/yii2/db/SchemaBuilderTrait.php';
  require_once '../vendor/yiisoft/yii2/db/MigrationInterface.php';
  require_once '../vendor/yiisoft/yii2/db/Migration.php';
  require_once '../vendor/craftcms/cms/src/db/Migration.php';
  require_once '../vendor/craftcms/cms/src/db/MigrationManager.php';
  $config = require_once '../vendor/craftcms/cms/src/config/app.php';
  echo 'Craft:', $config['version'], "\n";
}

if (file_exists('../vendor/laravel/framework/src/Illuminate/Foundation/Application.php')) {  // laravel
  $lines = file('../vendor/laravel/framework/src/Illuminate/Foundation/Application.php');
  $regexp = "/const VERSION = '(.*?)';/";
  $result = preg_grep($regexp, $lines);
  if ($result) {
    preg_match($regexp, array_values($result)[0], $matches);
    if ($matches) echo 'Laravel:', $matches[1], "\n";
  }
}

if (file_exists('app/bootstrap.php') || file_exists('../app/bootstrap.php')) {  // magento 2.3
  if (file_exists('app/bootstrap.php')) { // installed in docroot :-(
    require_once 'app/bootstrap.php';
  }
  if (file_exists('../app/bootstrap.php')) { // properly installed a level up
    require_once '../app/bootstrap.php';
  }

  $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, []);
  $objectManager = $bootstrap->getObjectManager();
  $productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
  echo 'Magento:', $productMetadata->getVersion(), "\n";
}
